package gulajava.teslibraryrests.modelan;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by Gulajava Ministudio on 2/1/16.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KandidatModel {

    private DataResults data;

    public KandidatModel() {
    }

    public DataResults getData() {
        return data;
    }

    public void setData(DataResults data) {
        this.data = data;
    }
}
