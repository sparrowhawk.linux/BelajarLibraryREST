package gulajava.teslibraryrests.internet.retrofits2;

import gulajava.teslibraryrests.modelan.KandidatModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Gulajava Ministudio on 2/8/16.
 */
public interface ApisRetro2 {

    //http://api.pemiluapi.org/calonpilkada/api/candidates?apiKey=8e67d2396454f98c370596b139194015&limit=100
    @GET("/calonpilkada/api/candidates")
    Call<KandidatModel> getDaftarKandidat(@Query("apiKey") String apikey, @Query("limit") String limitdata);


}
