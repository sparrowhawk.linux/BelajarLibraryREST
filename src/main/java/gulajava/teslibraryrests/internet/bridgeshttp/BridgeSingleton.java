package gulajava.teslibraryrests.internet.bridgeshttp;

import com.afollestad.bridge.Bridge;
import com.afollestad.bridge.conversion.JsonRequestConverter;
import com.afollestad.bridge.conversion.JsonResponseConverter;

import gulajava.teslibraryrests.internet.KonstanInternet;

/**
 * Created by Gulajava Ministudio on 2/2/16.
 */
public class BridgeSingleton {
    private static BridgeSingleton ourInstance;

    public static BridgeSingleton getInstance() {

        if (ourInstance == null) {
            ourInstance = new BridgeSingleton();
        }

        return ourInstance;
    }

    private BridgeSingleton() {

        Bridge.config().connectTimeout(10000);
        Bridge.config().readTimeout(10000);
        Bridge.config().defaultHeader("Content-Type", "application/json");
        Bridge.config().requestConverter("application/json", new JsonRequestConverter());
        Bridge.config().responseConverter("application/json", new JsonResponseConverter());

    }


    /**
     * API UNTUK DAFTAR KANDIDAT
     **/
    //http://api.pemiluapi.org/calonpilkada/api/candidates?apiKey=8e67d2396454f98c370596b139194015&limit=100
    public String getKandidatDaftar(String limit) {

        return KonstanInternet.ALAMATSERVER + "/calonpilkada/api/candidates?apiKey=" +
                KonstanInternet.APIKEYS + "&limit=" + limit;
    }

}
