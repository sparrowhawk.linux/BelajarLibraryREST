package gulajava.teslibraryrests.internet.okhttps;

import gulajava.teslibraryrests.internet.KonstanInternet;
import gulajava.teslibraryrests.internet.OkHttpKlienSingleton;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;

/**
 * Created by Gulajava Ministudio on 1/25/16.
 */
public class OkHttpNets {

    private static OkHttpNets ourInstance;
    private OkHttpClient mOkHttpClient;
    private String BASE_URL = "http://api.pemiluapi.org";


    public static OkHttpNets getInstance() {

        if (ourInstance == null) {
            ourInstance = new OkHttpNets();
        }

        return ourInstance;
    }

    private OkHttpNets() {

        mOkHttpClient = OkHttpKlienSingleton.getInstance().getOkHttpClient();
    }


    public Call getRequestKandidatPartai(String limit) {

        String urls = BASE_URL + ApisOkHttp.getKandidatDaftar(KonstanInternet.APIKEYS, limit);
        Request.Builder builderReqs = new Request.Builder();
        builderReqs.addHeader("Content-Type", "application/json");
        builderReqs.url(urls);
        Request request = builderReqs.build();

        return mOkHttpClient.newCall(request);
    }

}
